#!/usr/bin/env node
const fs = require('fs');
const path = require('path');
const package = require('../package.json');
const { tracelib } = require('../dist/tracelib.js');

// 输出数据
function outputData(result, outputPath) {
  if (outputPath) {
    fs.writeFile(path.resolve(process.cwd(), outputPath), JSON.stringify(result), 'utf-8', function(error) {
      console.log('写入成功');
    });
  } else {
    console.log(result);
  }
}

function run(argv) {
  if (argv[0] === '-v' || argv[0] === '--version') {
    console.log('  version is ' + package.version);
  } else if (argv[0] === '-h' || argv[0] === '--help') {
    console.log('  usage:\n');
    console.log('  -v --version                                 [show version]');
    console.log('  [logPath][getSummary][outputPath?]           Fetch total time-durations of scripting, rendering, painting from tracelogs.');
    console.log('  [logPath][getWarningCounts][outputPath?]     Fetch amount of forced synchronous layouts and styles as well as long recurring handlers.');
    console.log('  [logPath][getFPS][outputPath?]               Fetch frames per second.');
    console.log('  [logPath][getMemoryCounters][outputPath?]    Fetch data for JS Heap, Documents, Nodes, Listeners and GPU Memory from tracelogs.');
    console.log('  [logPath][getDetailStats][outputPath?]       Fetch data (timestamp and values) of scripting, rendering, painting from tracelogs.');
  } else if (argv.length >= 2) {
    const logData = require(path.resolve(process.cwd(), argv[0]));
    const type = argv[1];
    let result = '';

    switch (type) {
      case 'getSummary':
        result = tracelib.getSummary(logData);
        break;
      case 'getWarningCounts':
        result = tracelib.getWarningCounts(logData);
        break;
      case 'getFPS':
        result = tracelib.getFPS(logData);
        break;
      case 'getMemoryCounters':
        result = tracelib.getMemoryCounters(logData);
        break;
      case 'getDetailStats':
        result = tracelib.getDetailStats(logData);
        break;
      default:
        console.log(new Error('This method is not available yet.'));
        break;
    }

    outputData(result, argv[2]);
  }
}

run(process.argv.slice(2));
