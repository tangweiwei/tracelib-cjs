import Tracelib from 'tracelib';

export const getSummary = (traceLog) => {
  const tasks = new Tracelib(traceLog);
  return tasks.getSummary();
}

export const getWarningCounts = (traceLog) => {
  const tasks = new Tracelib(traceLog);
  return tasks.getWarningCounts();
}

export const getFPS = (traceLog) => {
  const tasks = new Tracelib(traceLog);
  return tasks.getFPS();
}

export const getMemoryCounters = (traceLog) => {
  const tasks = new Tracelib(traceLog);
  return tasks.getMemoryCounters();
}

export const getDetailStats = (traceLog) => {
  const tasks = new Tracelib(traceLog);
  return tasks.getDetailStats();
}

export default {
  getSummary,
  getWarningCounts,
  getFPS,
  getMemoryCounters,
  getDetailStats
}
