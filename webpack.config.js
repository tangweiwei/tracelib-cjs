'use strict'

const path = require('path');

module.exports = {
  mode: 'production',
  entry: {
    tracelib: './index.js'
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: '[name].js',
    library: '[name]',
    libraryTarget: 'commonjs'
  }
}
